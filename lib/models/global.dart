import 'package:flutter/material.dart';

Color darkBlue = Color(0xFF018197);

TextStyle buttonStyleBlue =
    TextStyle(color: Colors.blue, fontFamily: "Helvetica");
TextStyle productTitleStyle = TextStyle(
    fontSize: 20, fontFamily: "Helvetica", fontWeight: FontWeight.bold);
TextStyle whiteItalicTitleStyle = TextStyle(
    fontSize: 20,
    fontFamily: "Helvetica",
    fontStyle: FontStyle.italic,
    color: Colors.white);
